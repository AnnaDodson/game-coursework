# G54GAM Coursework

## Design and build game prototype in Unreal.

Game Overview and Core Mechanic:
Control the movement of a small droid through the levels collecting all the coins to 'buy' the key the next level. 
Finish the level within the bonus time to unlock secret bonus levels for more coins and lives.

Narrative:
Inspiration from a popular character from a popular film to inspire the character. Specific related stories within the game. Example the droid needs batteries to charge for lives. Overall aim of the game to return a piece of data to the rebel base.

First Level:
The first level gives the game aims and instructions. These are provided on screen to display to the player as they maneuver around the first level and get used to the control of the droid.
The setting is simple and no damage is explicitly trying to destroy the droid.
The key points in the level are clearly communicated, the key is on top of a prominent podium and the door has a big coin arrow pointing to it to show the player what to do, this is transferred across each level so they do not need to be shown a new rule.

Second Level:
Introduce higher challenge. The ability to kill the droid is introduced with a bridge over an abyss which would kill the droid.
The central bridge has thrusters turning off and on at regular intervals. The thrusters are communicated to the player by different texture floor and when the thrusters are active, the floor is a different colour. 
The bridges either side of the abyss make the game slightly easier as this is within the learning curve, as the first thruster generally won't kill the droid, as the bridge catches it. This allows the player to learn what the thrusters do and how to avoid them.
The key is shown on the bridge when the coins are collected so the player has to cross twice.

Bonus Level:
If the player completes level two within a time frame, a bonus level is unlocked.
The bonus level has no specific challenge and is desgined as a different asthetic, no frustration but to enjoy the level. It also introduces the player to new movements and allows them to collect more lives and coins.

Third Level:
Harder level prototype. Introduce moving platforms to get coins from and also thrusters with no safety bridges. If the droid falls off the platforms, the player will need to start from the beginning.
Adds frustration and challenge to the game. 

Potential Further Development:
Collect the score from bonus levels to unlock more lives or power ups (ability to pause time and stop moving objects for example)
Use the coins to 'buy' new challenges. For example, charter on a 'space ship' to get to the next level.
As the player gets better they can unlock further lives and potentially progress further in the game. Example, the harder the levels become it might be very likely the player will need more lives which are only available by completing time challenges. This will also further maintain game balance.

Refrences: 
Map and Character Template taken from Rolling Ball Game as a free Unreal project. Character Ball modified to improve movement.

Assets:
Unreal starter content graphics and free materials pack

Actor Rotation reference: 
https://wiki.unrealengine.com/Blueprint_Rotating_Movement_Component